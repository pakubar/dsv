defmodule Dsv.MixProject do
  use Mix.Project

  def project do
    [
      app: :dsv,
      version: "0.3.0",
      elixir: "~> 1.16",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "Dsv",
      source_url: "https://gitlab.com/pakubar/dsv",
      description: description(),
      package: package(),
      docs: [
        main: "how_to_validate",
        extras: ["README.md", "how_to_validate.md", "CHANGELOG.md": [title: "Changelog"]],
        groups_for_modules: [
          "Basic validators": [
            Dsv.All,
            Dsv.Any,
            Dsv.None,
            Dsv.Custom,
            Dsv.Date,
            Dsv.Equal,
            Dsv.Exclusion,
            Dsv.Format,
            Dsv.Inclusion,
            Dsv.Length,
            Dsv.NotEmpty,
            Dsv.Or,
            Dsv.At,
            Dsv.Email,
            Dsv.Number,
            Dsv.Type,
            Dsv.UUID,
            Dsv.Unique
          ],
          Protocols: [
            Dsv.Empty,
            FindAll,
            FindAny,
            ValueAt,
            Comparable,
            DataLength,
            DateComparator,
            Dsv.EqualProto,
            Iterable
          ],
          Behaviours: [
            Dsv.Comparator
          ],
          "Custom validator module": [
            Dsv.Validator
          ]
        ]
      ]
    ]
  end

  defp description() do
    "An elixir validation library"
  end

  defp package() do
    [
      files: ~w(lib mix.exs README.* LICENSE*),
      licenses: ["Apache"],
      links: %{"GitLab" => "https://gitlab.com/pakubar/dsv"}
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :eex]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:dialyxir, "~> 1.4.0", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.31.0", only: :dev, runtime: false},
      {:stream_data, "~> 1.0", only: :test, runtime: false}
    ]
  end
end
