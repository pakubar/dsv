# 0.3.0

### New features:
* UUID validator - check if input is a propert UUID

# 0.2.1
* Add changelog for 0.2.0

# 0.2.0

### New features:
* Binded values - possibility to bind values from validated data and use themn in the validator options.
* Dsv.Type validator - validate type of the input data
* Reusable pipeline validators - allow to create pipeline validator and then validate different data with the same pipeline validator.

# 0.1.1

### Fixes:
* Iterable protocol, default map implementation finish iteration with proper return value when no elemnts in map left
* Type `validators` spec
* Type spec for Dsv.Date.validate/2 function
* Dsv.Validator.generate_message function implementation with default errors argument for message function

### Tests:
* Dsv.Comparator implementation for Number
* Dsv.Empty - DateTime, Date, NaiveDateTime, Time, Float, empty List
* Iterable protocol - Map iteration, Tuple - element not found
* Permutations - test for permutations module

# 0.1.0
* Initial release
