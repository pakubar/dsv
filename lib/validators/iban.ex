defmodule CountryIban do
  defstruct [:length, :country]
end

defmodule Dsv.Iban do
  use Dsv.Validator

  @length_by_country %{
    "VA" => %CountryIban{length: 22, country: "Vatican City"},
    "VG" => %CountryIban{length: 24, country: "Virgin Island, British"},
    "GB" => %CountryIban{length: 22, country: "United Kingdom"},
    "AE" => %CountryIban{length: 23, country: "United Arab Emirates"},
    "UA" => %CountryIban{length: 29, country: "Ukraine"},
    "TR" => %CountryIban{length: 26, country: "Turkey"},
    "TN" => %CountryIban{length: 24, country: "Tunisia"},
    "CH" => %CountryIban{length: 21, country: "Switzerland"},
    "SE" => %CountryIban{length: 24, country: "Sweden"},
    "SD" => %CountryIban{length: 18, country: "Sudan"},
    "ES" => %CountryIban{length: 24, country: "Spain"},
    "SI" => %CountryIban{length: 19, country: "Slovenia"},
    "SK" => %CountryIban{length: 24, country: "Slovakia"},
    "SC" => %CountryIban{length: 31, country: "Seychelles"},
    "RS" => %CountryIban{length: 22, country: "Serbia"},
    "SA" => %CountryIban{length: 24, country: "Saudi Arabia"},
    "ST" => %CountryIban{length: 25, country: "São Tomé and Príncipe"},
    "SM" => %CountryIban{length: 27, country: "San Marino"},
    "LC" => %CountryIban{length: 32, country: "Saint Lucia"},
    "RO" => %CountryIban{length: 24, country: "Romania"},
    "QA" => %CountryIban{length: 29, country: "Qatar"},
    "PT" => %CountryIban{length: 25, country: "Portugal"},
    "PL" => %CountryIban{length: 24, country: "Poland"},
    "PS" => %CountryIban{length: 29, country: "Palestinian territories"},
    "PK" => %CountryIban{length: 24, country: "Pakistan"},
    "NO" => %CountryIban{length: 15, country: "Norway"},
    "MK" => %CountryIban{length: 19, country: "North Macedonia"},
    "NL" => %CountryIban{length: 18, country: "Netherlands"},
    "ME" => %CountryIban{length: 22, country: "Montenegro"},
    "MD" => %CountryIban{length: 24, country: "Moldova"},
    "MC" => %CountryIban{length: 27, country: "Monaco"},
    "MU" => %CountryIban{length: 30, country: "Mauritius"},
    "MR" => %CountryIban{length: 27, country: "Mauritania"},
    "MT" => %CountryIban{length: 31, country: "Malta"},
    "LU" => %CountryIban{length: 20, country: "Luxembourg"},
    "LT" => %CountryIban{length: 20, country: "Lithuania"},
    "LI" => %CountryIban{length: 21, country: "Liechtenstein"},
    "LY" => %CountryIban{length: 25, country: "Libya"},
    "LB" => %CountryIban{length: 28, country: "Lebanon"},
    "LV" => %CountryIban{length: 21, country: "Latvia"},
    "KW" => %CountryIban{length: 30, country: "Kuwait"},
    "XK" => %CountryIban{length: 20, country: "Kosovo"},
    "KZ" => %CountryIban{length: 20, country: "Kazakhstan"},
    "JO" => %CountryIban{length: 30, country: "Jordan"},
    "IT" => %CountryIban{length: 27, country: "Italy"},
    "IL" => %CountryIban{length: 23, country: "Israel"},
    "IE" => %CountryIban{length: 22, country: "Ireland"},
    "IQ" => %CountryIban{length: 23, country: "Iraq"},
    "IS" => %CountryIban{length: 26, country: "Iceland"},
    "HU" => %CountryIban{length: 28, country: "Hungary"},
    "GT" => %CountryIban{length: 28, country: "Guatemala"},
    "GL" => %CountryIban{length: 18, country: "Greenland"},
    "GR" => %CountryIban{length: 27, country: "Greece"},
    "GI" => %CountryIban{length: 23, country: "Gibraltar"},
    "DE" => %CountryIban{length: 22, country: "Germany"},
    "GE" => %CountryIban{length: 22, country: "Georgia"},
    "FR" => %CountryIban{length: 27, country: "France"},
    "FI" => %CountryIban{length: 18, country: "Finland"},
    "FO" => %CountryIban{length: 18, country: "Faroe Islands"},
    "EE" => %CountryIban{length: 20, country: "Estonia"},
    "SV" => %CountryIban{length: 28, country: "El salvador"},
    "EG" => %CountryIban{length: 29, country: "Egypt"},
    "TL" => %CountryIban{length: 23, country: "East Timor"},
    "DO" => %CountryIban{length: 28, country: "Dominican Republic"},
    "DK" => %CountryIban{length: 18, country: "Denmark"},
    "CZ" => %CountryIban{length: 24, country: "Czech Republic"},
    "CY" => %CountryIban{length: 28, country: "Cyprus"},
    "HR" => %CountryIban{length: 21, country: "Croatia"},
    "CR" => %CountryIban{length: 22, country: "Costa Rica"},
    "BG" => %CountryIban{length: 22, country: "Bulgaria"},
    "BR" => %CountryIban{length: 29, country: "Brazil"},
    "BA" => %CountryIban{length: 20, country: "Bosnia and Herzegovina"},
    "BE" => %CountryIban{length: 16, country: "Belgium"},
    "BY" => %CountryIban{length: 28, country: "Belarus"},
    "BH" => %CountryIban{length: 22, country: "Bahrain"},
    "AZ" => %CountryIban{length: 28, country: "Azerbaijan"},
    "AT" => %CountryIban{length: 20, country: "Austria"},
    "AD" => %CountryIban{length: 24, country: "Andorra"},
    "AL" => %CountryIban{length: 28, country: "Albania"}
  }

  message("Value <%= data %> is not a correct IBAN number.")

  @doc """
  The `valid?/2` function checks if the given value is the correct iban number.

  ## Parameters
    * `iban` - value to check

  ## Returns

  A boolean value:

  - `true` - given value is a correct iban
  - `false` - given value is not a correct iban

  ## Examples

    iex> Dsv.Iban.valid?("LU940108435278843264")
    true

    iex> Dsv.Iban.valid?("LU940108435278843263")
    false

    iex> Dsv.Iban.valid?("GT94363152336311486138899346")
    true

    iex> Dsv.Iban.valid?("GT99363152336311486138899346")
    false

    iex> Dsv.Iban.valid?("GT9363152336311486138899346")
    false

  """
  def valid?(iban) do
    with true <- check_length(iban),
         true <- check_checksum(iban),
         true <- check_iban_number_with_checksum(iban) do
      true
    else
      false -> false
    end
  end

  def valid?(iban, []), do: valid?(iban)

  @doc """
  The `validate/2` function checks if the given value is the correct iban number.

  ## Parameters
    * `iban` - value to check

  ## Returns

  A boolean value:

  - `:ok` - given value is a correct iban
  - `{:error, error_message}` - given value is not a correct iban

  ## Examples

    iex> Dsv.Iban.validate("LU940108435278843264")
    :ok

    iex> Dsv.Iban.validate("LU940108435278843263")
    {:error, "Value LU940108435278843263 is not a correct IBAN number."}

    iex> Dsv.Iban.validate("GT94363152336311486138899346")
    :ok

    iex> Dsv.Iban.validate("GT99363152336311486138899346")
    {:error, "Value GT99363152336311486138899346 is not a correct IBAN number."}

    iex> Dsv.Iban.validate("GT9363152336311486138899346")
    {:error, "Value GT9363152336311486138899346 is not a correct IBAN number."}

  """
  def validate(iban, options), do: super(iban, options)

  defp check_iban_number_with_checksum(iban) do
    checksum_number = String.slice(iban, 2..3)
    country_code = String.slice(iban, 0..1)

    iban
    |> String.graphemes()
    |> Enum.drop(4)
    |> Enum.join("")
    |> (&(&1 <> country_code <> "00")).()
    |> (&(&1 |> replace_with_digits)).()
    |> String.to_integer()
    |> rem(97)
    |> (&(98 - &1)).()
    |> Integer.to_string()
    |> String.pad_leading(2, "0")
    |> (&(&1 == checksum_number)).()
  end

  defp get_country_code(iban) when is_bitstring(iban), do: iban |> String.slice(0..1)

  defp country_iban_length(""), do: nil
  defp country_iban_length(country_code), do: Map.get(@length_by_country, country_code)

  defp length_equal?(_iban, nil), do: false
  defp length_equal?(iban, %CountryIban{length: length}), do: String.length(iban) == length

  defp check_length(iban),
    do:
      iban
      |> get_country_code()
      |> country_iban_length()
      |> (&length_equal?(iban, &1)).()

  defp check_checksum(iban) do
    iban = swap_first_four_graphemes(iban)
    iban = replace_with_digits(iban)
    Integer.mod(String.to_integer(iban), 97) == 1
  end

  defp swap_first_four_graphemes(iban), do: repeat(iban, 4, &move_first_grapheme_to_the_end/1)

  defp move_first_grapheme_to_the_end(iban) do
    {head, tail} = String.next_codepoint(iban)
    tail <> head
  end

  defp replace_with_digits(iban),
    do:
      iban
      |> String.graphemes()
      |> Enum.map(&map_letter/1)
      |> Enum.join()

  defp map_letter(<<grapheme::utf8>>) do
    if ascii_case_letter?(grapheme),
      do: Integer.to_string(grapheme - 55),
      else: List.to_string([grapheme])
  end

  defp ascii_case_letter?(grapheme), do: grapheme in ?A..?Z

  defp repeat(iban, 0, _function), do: iban
  defp repeat(iban, times, function), do: repeat(function.(iban), times - 1, function)
end
