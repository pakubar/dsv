defmodule Dsv.UUID do
  use Dsv.Validator,
    message_key_mapper: fn
      [{:format, option}] -> [option]
      options -> [options]
    end

  @moduledoc """
  Check if value is correct UUID
  """

  @format ~r/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/
  @urn_format ~r/^urn:uuid:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/

  message({:basic, "Wrong uuid format"})
  message({:urn, "Wrong urn uuid format"})

  @doc """
  The `valid/2` function check if provided value is proper UUID in specified format.

  ## Parameters

  * `value` (string) - The value to be checked for proper uuid format.
  * `format` (:basic|:urn) - one of the value: `:basic` or `:urn`.

  ## Returns

  A boolean value:

  - `true` if `value` is proper uuid (in the defined format)
  - `false` if `value` is not proper uuid or is proper uuid in different format.


  ## Example
  	iex> Dsv.UUID.valid?("19757040-cdbc-11ee-a506-0242ac120002")
  	:true

  	iex> Dsv.UUID.valid?("caeff9d7-ba9e-434e-9813-39d77ebb3e0d", format: :basic)
  	:true

  	iex> Dsv.UUID.valid?("urn:uuid:19757040-cdbc-11ee-a506-0242ac120002",:urn)
  	:true

  	iex> Dsv.UUID.valid?("urn:uuid:caeff9d7-ba9e-434e-9813-39d77ebb3e0d", format: :urn)
  	:true

  	iex> Dsv.UUID.valid?("119757040-cdbc-11ee-a506-0242ac120002")
  	:false

  	iex> Dsv.UUID.valid?("caeff9d7-ba9e-434e-9813-39d7uebb3e0d", format: :basic)
  	:false

  	iex> Dsv.UUID.valid?("urn:uuid:1975A040-cdbc-11ee-a506-0242ac120002", :urn)
  	:false

  	iex> Dsv.UUID.valid?("urn:uuid:caeff9D7-ba9e-434e-9813-39d77ebb3e0d", format: :urn)
  	:false

  	iex> Dsv.UUID.valid?("caeff9d7-ba9e-434e-9813-39d77ebb3e0d", :urn)
  	:false
  """
  def valid?(uuid, format \\ :basic)
  def valid?(uuid, []), do: valid?(uuid, :basic)
  def valid?(uuid, :basic), do: valid?(uuid, format: :basic)
  def valid?(uuid, :urn), do: valid?(uuid, format: :urn)
  def valid?(uuid, format: :basic), do: Regex.match?(@format, uuid)
  def valid?(uuid, format: :urn), do: Regex.match?(@urn_format, uuid)

  @doc """
  The `validate/2` function check if provided value is proper UUID in specified format.

  ## Parameters

  * `value` (string) - The value to be checked for proper uuid format.
  * `format` (:basic|:urn) - one of the value: `:basic` or `:urn`.

  ## Returns

  A boolean value:

  - `:ok` if `value` is proper uuid (in the defined format)
  - `{:error, message}` if `value` is not proper uuid or is proper uuid in different format.


  ## Example
  	iex> Dsv.UUID.validate("19757040-cdbc-11ee-a506-0242ac120002")
  	:ok

  	iex> Dsv.UUID.validate("caeff9d7-ba9e-434e-9813-39d77ebb3e0d", format: :basic)
  	:ok

  	iex> Dsv.UUID.validate("urn:uuid:19757040-cdbc-11ee-a506-0242ac120002", :urn)
  	:ok

  	iex> Dsv.UUID.validate("urn:uuid:caeff9d7-ba9e-434e-9813-39d77ebb3e0d", format: :urn)
  	:ok

  	iex> Dsv.UUID.validate("197g7040-cdbc-11ee-a506-0242ac120002")
  	{:error, "Wrong uuid format"}

  	iex> Dsv.UUID.validate("caeff9d7-bi9e-434e-9813-39d77ebb3e0d", format: :basic)
  	{:error, "Wrong uuid format"}

  	iex> Dsv.UUID.validate("urn:uuid:19757040-ndbc-11ee-a506-0242ac120002", :urn)
  	{:error, "Wrong urn uuid format"}

  	iex> Dsv.UUID.validate("urn:uuid:caeff9d7-ba9e-434e-9813-39d77ebb3e0d1", format: :urn)
  	{:error, "Wrong urn uuid format"}
  """
  def validate(uuid, []), do: super(uuid, :basic)
  def validate(uuid, options), do: super(uuid, options)
end
