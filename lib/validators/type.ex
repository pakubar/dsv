defmodule Dsv.Type do
  use Dsv.Validator

  @moduledoc """
  Ensure that input data are of the typed defined in the validator.
  """

  message(fn _data, options -> "Wrong type. Expected type is #{options}" end)

  @doc """
  Ensure that the data are of the type defined as a second argument.

  Returns `:true` if data are of the type defined as a second argument, otherwise, returns `:false`.

  ## Example

  	iex> Dsv.Type.valid?("abcd", :string)
  	:true

  	iex> Dsv.Type.valid?(1, :string)
  	:false

  	iex> Dsv.Type.valid?(:true, :boolean)
  	:true

  	iex> Dsv.Type.valid?(:hello, :boolean)
  	:false

  	iex> Dsv.Type.valid?(:test_atom, :atom)
  	:true

  	iex> Dsv.Type.valid?("test text", :atom)
  	:false

  	iex> Dsv.Type.valid?(134, :number)
  	:true

  	iex> Dsv.Type.valid?(12.4, :number)
  	:true

  	iex> Dsv.Type.valid?(%{}, :number)
  	:false

  	iex> Dsv.Type.valid?(2, :integer)
  	:true

  	iex> Dsv.Type.valid?(3.2, :integer)
  	:false

  	iex> Dsv.Type.valid?(3.2, :float)
  	:true

  	iex> Dsv.Type.valid?(3, :float)
  	:false

  	iex> Dsv.Type.valid?([1, 2, 4], :list)
  	:true

  	iex> Dsv.Type.valid?({1, 2, 4}, :list)
  	:false

  	iex> Dsv.Type.valid?({1, 2, 4}, :tuple)
  	:true

  	iex> Dsv.Type.valid?([1, 2, 4], :tuple)
  	:false

  	iex> Dsv.Type.valid?(%{a: :b}, :map)
  	:true

  	iex> Dsv.Type.valid?({:a, :b}, :map)
  	:false

  	iex> Dsv.Type.valid?([a: 1, b: 2], :keyword_list)
  	:true

  	iex> Dsv.Type.valid?([:a, :b], :keyword_list)
  	:false

  	iex> Dsv.Type.valid?(~U[2020-01-01 01:01:01.01Z], :date_time)
  	:true

  	iex> Dsv.Type.valid?(~D[2020-01-01], :date_time)
  	:false

  	iex> Dsv.Type.valid?(~D[2020-01-01], :date)
  	:true

  	iex> Dsv.Type.valid?(~U[2020-01-01 01:01:01.01Z], :date)
  	:false

  	iex> Dsv.Type.valid?(~T[01:01:01], :time)
  	:true

  	iex> Dsv.Type.valid?(~D[2020-01-01], :time)
  	:false

  	iex> Dsv.Type.valid?(~N[2020-01-01 01:01:01], :naive_date_time)
  	:true

  	iex> Dsv.Type.valid?(~U[2020-01-01 01:01:01.01Z], :naive_date_time)
  	:false
  """
  def valid?(data, :string) when is_binary(data), do: true
  def valid?(_, :string), do: false

  def valid?(data, :boolean) when is_boolean(data), do: true
  def valid?(_, :boolean), do: false

  def valid?(data, :atom) when is_atom(data), do: true
  def valid?(_, :atom), do: false

  def valid?(data, :number) when is_number(data), do: true
  def valid?(_, :number), do: false

  def valid?(data, :integer) when is_integer(data), do: true
  def valid?(_, :integer), do: false

  def valid?(data, :float) when is_float(data), do: true
  def valid?(_, :float), do: false

  def valid?(data, :list) when is_list(data), do: true
  def valid?(_, :list), do: false

  def valid?(data, :tuple) when is_tuple(data), do: true
  def valid?(_, :tuple), do: false

  def valid?(data, :map) when is_map(data), do: true
  def valid?(_, :map), do: false

  def valid?(data, :keyword_list), do: Keyword.keyword?(data)

  def valid?(%DateTime{}, :date_time), do: true
  def valid?(_, :date_time), do: false

  def valid?(%Date{}, :date), do: true
  def valid?(_, :date), do: false

  def valid?(%Time{}, :time), do: true
  def valid?(_, :time), do: false

  def valid?(%NaiveDateTime{}, :naive_date_time), do: true
  def valid?(_, :naive_date_time), do: false

  def valid?(_data, :any), do: true
  def valid?(data, _options), do: valid?(data, :any)

  @doc """
  Ensure that the data are of the type defined as a second argument.

  Returns `:ok` if data are of the type defined as a second argument, otherwise, returns `{:error, message}`.

  ## Example

  	iex> Dsv.Type.validate("abcd", :string)
  	:ok

  	iex> Dsv.Type.validate(1, :string)
  	{:error, "Wrong type. Expected type is string"}

  	iex> Dsv.Type.validate(:true, :boolean)
  	:ok

  	iex> Dsv.Type.validate(:hello, :boolean)
  	{:error, "Wrong type. Expected type is boolean"}

  	iex> Dsv.Type.validate(:test_atom, :atom)
  	:ok

  	iex> Dsv.Type.validate("test text", :atom)
  	{:error, "Wrong type. Expected type is atom"}

  	iex> Dsv.Type.validate(134, :number)
  	:ok

  	iex> Dsv.Type.validate(12.4, :number)
  	:ok

  	iex> Dsv.Type.validate(%{}, :number)
  	{:error, "Wrong type. Expected type is number"}

  	iex> Dsv.Type.validate(2, :integer)
  	:ok

  	iex> Dsv.Type.validate(3.2, :integer)
  	{:error, "Wrong type. Expected type is integer"}

  	iex> Dsv.Type.validate(3.2, :float)
  	:ok

  	iex> Dsv.Type.validate(3, :float)
  	{:error, "Wrong type. Expected type is float"}

  	iex> Dsv.Type.validate([1, 2, 4], :list)
  	:ok

  	iex> Dsv.Type.validate({1, 2, 4}, :list)
  	{:error, "Wrong type. Expected type is list"}

  	iex> Dsv.Type.validate({1, 2, 4}, :tuple)
  	:ok

  	iex> Dsv.Type.validate([1, 2, 4], :tuple)
  	{:error, "Wrong type. Expected type is tuple"}

  	iex> Dsv.Type.validate(%{a: :b}, :map)
  	:ok

  	iex> Dsv.Type.validate({:a, :b}, :map)
  	{:error, "Wrong type. Expected type is map"}

  	iex> Dsv.Type.validate([a: 1, b: 2], :keyword_list)
  	:ok

  	iex> Dsv.Type.validate([:a, :b], :keyword_list)
  	{:error, "Wrong type. Expected type is keyword_list"}

  	iex> Dsv.Type.validate(~U[2020-01-01 01:01:01.01Z], :date_time)
  	:ok

  	iex> Dsv.Type.validate(~D[2020-01-01], :date_time)
  	{:error, "Wrong type. Expected type is date_time"}

  	iex> Dsv.Type.validate(~D[2020-01-01], :date)
  	:ok

  	iex> Dsv.Type.validate(~U[2020-01-01 01:01:01.01Z], :date)
  	{:error, "Wrong type. Expected type is date"}

  	iex> Dsv.Type.validate(~T[01:01:01], :time)
  	:ok

  	iex> Dsv.Type.validate(~D[2020-01-01], :time)
  	{:error, "Wrong type. Expected type is time"}

  	iex> Dsv.Type.validate(~N[2020-01-01 01:01:01], :naive_date_time)
  	:ok

  	iex> Dsv.Type.validate(~U[2020-01-01 01:01:01.01Z], :naive_date_time)
  	{:error, "Wrong type. Expected type is naive_date_time"}
  """
  def validate(data, options), do: super(data, options)
end
