defprotocol Dsv.EqualProto do
  @moduledoc """
  A protocol to check if two values are equal.
  This protocol is used by the `Dsv.Equal` validator.


  You can implement custom equality checks for other data types by providing specific protocol implementations.

  ## Example

      defmodule User do
        defstruct [:first_name, :last_name, :age, :id_number]
      end

      defimpl Equal, for: User do
        def equal?(%User{first_name: first_name1, last_name: last_name1, age: age1, id_number: id_number1}, %User{first_name: first_name2, last_name: last_name2, age: age2, id_number: id_number2}) do
          first_name1 === first_name2 and last_name1 === last_name2 and age1 === age2 and id_number1 === id_number2
        end
      end
  """

  @doc """
  Checks if two values are equal.

  ## Parameters

  * `a` - The first value to compare.
  * `b` - The second value to compare.

  ## Returns

  - `true` if `a` and `b` are considered equal.
  - `false` otherwise.

  """
  @fallback_to_any true
  def is_equal(data, options)
end

defimpl Dsv.EqualProto, for: Any do
  def is_equal(data, options), do: data === options
end
