defmodule Dsv.Unique do
  use Dsv.Validator

  @moduledoc """
  `Dsv.Unique` validator provides functions to checks that collection contains only unique elements.
  """
  alias ElixirSense.Providers.Suggestion.Reducers.Returns

  message("Not all elements are unique.")

  @doc """
  Ensure that all elements in the collection are unique.

  Returns `:true` if all elements of the collection are unique; otherwise, returns `:false`.

  ## Example
    iex> Dsv.Unique.valid?([1, 2, 3, 4, 9, 0, 12])
    :true

    iex> Dsv.Unique.valid?([1, 2, 4, 4, 9, 0, 12])
    :false

    iex> Dsv.Unique.valid?(["hello", "Hello", :hello])
    :true

    iex> Dsv.Unique.valid?(["hello", :hello, "Hello", :hello])
    :false

    iex> Dsv.Unique.valid?(%{"key" => "value", "next_key" => "next_value"})
    :true
  """
  def valid?(data), do: Enum.uniq(data) |> Enum.count() == Enum.count(data)
  def valid?(data, _options), do: valid?(data)

  @doc """
  Ensure that all elements in the collection are unique.

  Returns `:ok` if all elements of the collection are unique; otherwise, returns `{:error, "Not all elements are unique."}`.

  ## Example
    iex> Dsv.Unique.validate([1, 2, 3, 4, 9, 0, 12])
    :ok

    iex> Dsv.Unique.validate([1, 2, 4, 4, 9, 0, 12])
    {:error, "Not all elements are unique."}

    iex> Dsv.Unique.validate(["hello", "Hello", :hello])
    :ok

    iex> Dsv.Unique.validate(["hello", :hello, "Hello", :hello])
    {:error, "Not all elements are unique."}

    iex> Dsv.Unique.validate(%{"key" => "value", "next_key" => "next_value"})
    :ok
  """
  def validate(data, []), do: super(data, [])
  def validate(data, options), do: super(data, options)
end
