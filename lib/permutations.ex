defmodule Permutations do
  @moduledoc false

  def permutations(key) when is_atom(key), do: [[key]]

  def permutations([]), do: [[]]

  def permutations(list) when is_list(list) do
    for head <- list, tail <- permutations(list -- [head]), do: [head | tail]
  end
end
