defmodule ValidationResult do
  @moduledoc false

  defstruct [:result, :path]
end

defmodule ValidatorPipeline do
  alias ValidatorElements.Validator, as: VInfo
  import ValidatorElements, only: [go_through: 2]

  @moduledoc false

  def run_validation(data, validators, arguments \\ Keyword.new()) do
    binded_values = Keyword.get(arguments, :binded_values, nil)

    result_view =
      Keyword.get(arguments, :result_view, &ValidatorPipeline.ResultMapper.map_result/1)

    go_through(validators, data)
    |> Enum.map(fn validator_info -> run_validator_for_field(validator_info, binded_values) end)
    |> result_view.()
    |> ValidationSteps.response()
  end

  defp option_validate(data, validator, binded_values) do
    {name, options} = validator

    validator = ValidatorRegistry.get_validator(name)

    case binded_values do
      nil -> validator.validate(data, options)
      _ -> validator.validate(data, options, binded_values)
    end
  end

  def run_validator_for_field(
        %VInfo{validators: validators, path: path, data: data},
        binded_values
      ),
      do:
        validators
        |> Enum.map(fn validator -> option_validate(data, validator, binded_values) end)
        |> Enum.filter(&(&1 != :ok))
        |> Enum.map(fn {:error, error} -> error end)
        |> (&if(&1 == [], do: :ok, else: &1)).()
        |> (&%ValidationResult{result: &1, path: path}).()
end

defmodule ValidatorPipeline.ResultMapper do
  @moduledoc false

  def list_result(results) when is_list(results),
    do:
      results
      |> Enum.filter(fn %ValidationResult{result: result} -> result != :ok end)
      |> Enum.map(fn %ValidationResult{result: result, path: path} -> {path, result} end)
      |> Enum.reduce([], fn elem, acc -> acc ++ [elem] end)

  def map_result(results) when is_list(results),
    do:
      results
      |> Enum.filter(fn %ValidationResult{result: result} -> result != :ok end)
      |> Enum.reduce(%{}, fn %ValidationResult{result: result, path: path}, acc ->
        MapToolbox.put_nested(acc, path, result)
      end)

  def simple_result(results) when is_list(results),
    do:
      results
      |> Enum.filter(fn %ValidationResult{result: result} -> result != :ok end)
      |> Enum.map(fn %ValidationResult{result: result} -> result end)
      |> Enum.at(0, :ok)
end
