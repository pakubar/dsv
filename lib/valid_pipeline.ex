defmodule ValidPipeline do
  alias ValidatorElements.Validator, as: VInfo
  import ValidatorElements, only: [go_through: 2]

  @moduledoc false

  def run_validation(data, validators, binded_values \\ nil) do
    go_through(validators, data)
    |> Enum.flat_map(&zip_data_wiht_validators/1)
    |> Enum.map(fn {validator, data} -> option_valid?(data, validator, binded_values) end)
    |> Enum.all?(&(&1 == true))
  end

  defp option_valid?(data, validator, binded_values) do
    {name, options} = validator

    validator = ValidatorRegistry.get_validator(name)

    case binded_values do
      nil -> validator.valid?(data, options)
      _ -> validator.valid?(data, options, binded_values)
    end
  end

  defp zip_data_wiht_validators(%VInfo{validators: validators, data: data}),
    do: Enum.zip(validators, Stream.cycle([data]))
end
