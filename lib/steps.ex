defmodule ValidationSteps do
  @moduledoc false

  def response(data) when data == %{}, do: :ok
  def response(%{} = data), do: {:error, data}
  def response([]), do: :ok
  def response(data) when is_list(data), do: {:error, data}
  def response(:ok), do: :ok

  def group_errors({path, errors}, acc), do: Map.put(acc, path, errors)

  def path_validators_to_map_validators(validators) when is_list(validators) do
    validators
    |> Enum.map(&unpack_path_and_validators/1)
    |> Enum.reduce(%{}, fn {path, path_validators}, acc ->
      MapToolbox.put_nested(acc, path, path_validators)
    end)
  end

  def unpack_path_and_validators(options) do
    case options do
      [{:path, path} | validators] -> {path, validators}
      {path, validators} -> {[path], validators}
      [path | validators] -> {[path], validators}
    end
  end
end
