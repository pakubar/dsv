defmodule MapToolbox do
  @moduledoc false

  @doc """
  Put `value` into the map based on the `path` where `path` is a list containing names of nested map elements.

  `map` is the `Map` to put value into it. If `map` is `nil` then a new `Map` will be created.

  `path` is the list of keys under which `value` should be put.

  `value` is the value to put into the map

  ## Examples

  No map is defined. A new one will be created and returned.

      iex> MapToolbox.put_nested(nil, [:a, :b, :c, :d], "test")
      %{a: %{b: %{c: %{d: "test"}}}}

  An empty map can be provided.

      iex> MapToolbox.put_nested(%{}, [:a], 1)
      %{a: 1}

  A map with predefined values can be provided.

      iex>  MapToolbox.put_nested(%{a: %{b: %{}}}, [:a, :b, :c], "value")
      %{a: %{b: %{c: "value"}}}

      iex>  MapToolbox.put_nested(%{a: %{b: %{}}}, [:a, :b, :c, :d], "value")
      %{a: %{b: %{c: %{d: "value"}}}}

  """
  def put_nested(nil, path, value) when is_list(path) do
    path |> List.foldr(value, fn key, acc -> Map.put(%{}, key, acc) end)
  end

  def put_nested(%{} = map, [head | tail], value) do
    put_nested(Map.get(map, head), tail, value) |> (&Map.put(map, head, &1)).()
  end

  def put_nested(elem, [], value) when is_list(elem) and is_list(value) do
    elem ++ value
  end
end
