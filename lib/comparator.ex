defmodule Dsv.Comparator do
  @moduledoc """
  Specify the function that needs to be implemented by any validator used for comparing two elements from the validator input.
  """

  @doc """
  Create validator options from value and options.
  """

  @callback to_comparator(atom(), any()) :: any() | {:error, String.t()}
end
