defmodule ValidatorElements do
  @moduledoc false

  defmodule Validator do
    @moduledoc false
    defstruct [:validators, :data, :path]
  end

  def go_through(%Dsv.Validators{validators: validators}, data_to_validate),
    do: path_validators_to_list(validators, data_to_validate)

  def go_through(validators, data_to_validate) when is_list(validators),
    do: [%Validator{validators: validators, data: data_to_validate, path: []}]

  def go_through(validators, data_to_validate, path \\ [])

  def go_through(%{} = validators, data_to_validate, path),
    do:
      validators
      |> Enum.map(fn {name, nested_validators} ->
        go_through(nested_validators, get_element(data_to_validate, name), path ++ [name])
      end)
      |> Enum.reduce([], fn elem, acc -> acc ++ elem end)

  def go_through(validators, data_to_validate, path) when is_list(validators),
    do: [%Validator{validators: validators, data: data_to_validate, path: path}]

  defp path_validators_to_list(validators, data_to_validate) when is_list(validators),
    do:
      validators
      |> Enum.map(&unpack_path_and_validators_from_keywordlist/1)
      |> Enum.map(fn {path, validators_definition} ->
        %Validator{
          validators: validators_definition,
          data: get_nested_element(path, data_to_validate),
          path: path
        }
      end)

  defp get_nested_element([], data), do: data

  defp get_nested_element([current_element | []], data), do: get_element(data, current_element)

  defp get_nested_element([current_element | path], data),
    do: get_nested_element(path, get_element(data, current_element))

  defp get_element(%{} = data, field), do: Map.get(data, field)
  defp get_element(data, position) when is_integer(position), do: ValueAt.at(data, position)

  defp get_element(data, position) when is_bitstring(position),
    do: ValueAt.at(data, String.to_integer(position))

  defp get_element(data, position) when is_atom(position),
    do: ValueAt.at(data, String.to_integer(Atom.to_string(position)))

  defp unpack_path_and_validators_from_keywordlist(list) when is_list(list),
    do: Keyword.pop(list, :path)
end
