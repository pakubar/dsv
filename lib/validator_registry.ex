defmodule ValidatorRegistry do
  @validators Map.merge(
                %{
                  length: Dsv.Length,
                  not_empty: Dsv.NotEmpty,
                  date: Dsv.Date,
                  number: Dsv.Number,
                  in: Dsv.Inclusion,
                  not_in: Dsv.Exclusion,
                  format: Dsv.Format,
                  equal: Dsv.Equal,
                  custom: Dsv.Custom,
                  position: Dsv.At,
                  any: Dsv.Any,
                  all: Dsv.All,
                  none: Dsv.None,
                  email: Dsv.Email,
                  or: Dsv.Or,
                  type: Dsv.Type,
                  uuid: Dsv.UUID,
                  iban: Dsv.Iban,
                  unique: Dsv.Unique
                },
                Application.get_env(:dsv, :validators, %{})
              )

  def get_validator(name) when is_atom(name), do: @validators[name]
end
