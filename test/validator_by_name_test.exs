# #TODO -> Check format of validators. Example [[complex_kye, one, two, three, number: [gt:10, lt: 30]]] has wrong parenthesis and will use validators as part ot the key. Error should be return in this case, that there is no validator for the given key.

defmodule User do
  defstruct [:name, :age, :programming_languages, :skills]
end

defmodule Dsv.SimpleValueTest do
  use ExUnit.Case

  test "validate string value, no error message" do
    assert Dsv.validate("Test", length: [min: 2, max: 4]) == :ok
  end

  test "validate string value, return default error message" do
    assert Dsv.validate("Test", length: [min: 2, max: 3]) ==
             {:error,
              ["Value \"Test\" has wrong length. Minimum lenght is 2, maximum length is 3"]}
  end

  test "validate string value, return custom error message" do
    assert Dsv.validate("Test",
             length: [min: 2, max: 3],
             message:
               "Length of the value <%= data %> must be between <%= options[:length][:min] %> and <%= options[:length][:max] %>"
           ) == {:error, "Length of the value Test must be between 2 and 3"}
  end

  test "validate string value, return custom error message from function" do
    assert Dsv.validate("Test",
             length: [min: 2, max: 3],
             message: fn _data, _options, errors -> errors end
           ) ==
             {:error,
              ["Value \"Test\" has wrong length. Minimum lenght is 2, maximum length is 3"]}
  end

  test "validate map value with equal validator" do
    assert Dsv.validate(%{equal: "Test"}, or: [length: [min: 5], equal: %{equal: "Test"}]) == :ok
  end
end

defmodule Dsv.NestedValidatorMapTest do
  use ExUnit.Case

  import Dsv, only: [paths: 1]

  doctest Dsv.Number
  doctest Dsv.Length
  doctest DataLength
  doctest Dsv.Date
  doctest Dsv.Format
  doctest Dsv.NotEmpty
  doctest Dsv.Empty
  doctest Dsv.Equal
  doctest Dsv.Exclusion
  doctest Dsv.Inclusion
  doctest Dsv.Custom
  doctest Dsv.All
  doctest Dsv.Any
  doctest Dsv.At
  doctest Dsv.Email
  doctest Dsv.Or
  doctest ValueAt
  doctest MapToolbox
  doctest DateConverter
  doctest Dsv.None
  doctest Iterable
  doctest Dsv.Type
  doctest Dsv.UUID
  doctest Dsv.Iban
  doctest Dsv.Unique

  setup_all do
    {:ok,
     data_to_validate: %{
       "name" => "Bartek",
       "age" => 36,
       "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
       "skills" => %{
         :programming => 100,
         :cooking => 60
       }
     }}
  end

  test "validate data stored in map - no errors", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          ["name", length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/],
          ["age", number: [gt: 10, lt: 100]]
        ])
      )

    assert res == :ok
  end

  test "validate data stored in map - no errors - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["name"], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 100])
      |> Dsv.validate()

    assert res == :ok
  end

  test "validate data stored in map - no errors - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["name"], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 100])
      |> Dsv.validate(state[:data_to_validate])

    assert res == :ok
  end

  test "validate data stored in map, validators list as keyword list - no errors" do
    res =
      Dsv.validate(
        %{
          :name => "Bartek",
          :age => 36,
          "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
          "skills" => %{
            :programming => 100,
            :cooking => 60
          }
        },
        paths(
          name: [length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/],
          age: [number: [gt: 10, lt: 100]]
        )
      )

    assert res == :ok
  end

  test "validate data stored in map, validators list as keyword list - no errors - pipeline" do
    data = %{
      :name => "Bartek",
      :age => 36,
      "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
      "skills" => %{
        :programming => 100,
        :cooking => 60
      }
    }

    res =
      Dsv.validation(data)
      |> Dsv.add_validator([:name], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator([:age], number: [gt: 10, lt: 100])
      |> Dsv.validate()

    assert res == :ok
  end

  test "validate data stored in map, validators list as keyword list - no errors - reusable pipeline" do
    data = %{
      :name => "Bartek",
      :age => 36,
      "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
      "skills" => %{
        :programming => 100,
        :cooking => 60
      }
    }

    res =
      Dsv.validation()
      |> Dsv.add_validator([:name], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator([:age], number: [gt: 10, lt: 100])
      |> Dsv.validate(data)

    assert res == :ok
  end

  test "validate list equality - true", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          ["programming_languages", equal: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"]]
        ])
      )

    assert res == :ok
  end

  test "validate list equality - true - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["programming_languages"],
        equal: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"]
      )
      |> Dsv.validate()

    assert res == :ok
  end

  test "validate list equality - true - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["programming_languages"],
        equal: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"]
      )
      |> Dsv.validate(state[:data_to_validate])

    assert res == :ok
  end

  test "validate or - true", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          [
            "name",
            or: [[length: [min: 3], format: ~r/Rab.*/], [length: [max: 10], format: ~r/.*ek/]]
          ]
        ])
      )

    assert res == :ok
  end

  test "validate or - true - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["name"],
        or: [[length: [min: 3], format: ~r/Rab.*/], [length: [max: 10], format: ~r/.*ek/]]
      )
      |> Dsv.validate()

    assert res == :ok
  end

  test "validate or - true - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["name"],
        or: [[length: [min: 3], format: ~r/Rab.*/], [length: [max: 10], format: ~r/.*ek/]]
      )
      |> Dsv.validate(state[:data_to_validate])

    assert res == :ok
  end

  test "validate or - false", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          [
            "name",
            or: [[length: [min: 3], format: ~r/Rab.*/], [length: [max: 10], format: ~r/.*ka/]]
          ]
        ])
      )

    assert res == {:error, %{"name" => ["At least one validator must be valid."]}}
  end

  test "validate or - false - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["name"],
        or: [[length: [min: 3], format: ~r/Rab.*/], [length: [max: 10], format: ~r/.*ka/]]
      )
      |> Dsv.validate()

    assert res == {:error, %{"name" => ["At least one validator must be valid."]}}
  end

  test "validate or - false - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["name"],
        or: [[length: [min: 3], format: ~r/Rab.*/], [length: [max: 10], format: ~r/.*ka/]]
      )
      |> Dsv.validate(state[:data_to_validate])

    assert res == {:error, %{"name" => ["At least one validator must be valid."]}}
  end

  test "validate list equality - false", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          [
            "programming_languages",
            equal: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin", "Nim"]
          ]
        ])
      )

    assert res == {:error, %{"programming_languages" => ["Values must be equal"]}}
  end

  test "validate list equality - false - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["programming_languages"],
        equal: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin", "Nim"]
      )
      |> Dsv.validate()

    assert res == {:error, %{"programming_languages" => ["Values must be equal"]}}
  end

  test "validate list equality - false - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["programming_languages"],
        equal: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin", "Nim"]
      )
      |> Dsv.validate(state[:data_to_validate])

    assert res == {:error, %{"programming_languages" => ["Values must be equal"]}}
  end

  test "validate data stored in map - errors", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          ["name", length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          ["age", not_empty: true, number: [gt: 10, lt: 30]]
        ])
      )

    assert res ==
             {:error,
              %{
                "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                "name" => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ]
              }}
  end

  test "validate data stored in map - errors - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["name"],
        type: :string,
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["age"], type: :integer, not_empty: true, number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["programming_languages"], type: :list)
      |> Dsv.add_validator(["skills"], type: :map)
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                "name" => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ]
              }}
  end

  test "validate data stored in map - errors - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["name"],
        type: :string,
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["age"], type: :integer, not_empty: true, number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["programming_languages"], type: :list)
      |> Dsv.add_validator(["skills"], type: :map)
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                "name" => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ]
              }}
  end

  test "valid? data - true", state do
    res =
      Dsv.valid?(
        state[:data_to_validate],
        paths([
          ["name", length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/],
          ["age", number: [gt: 10, lt: 100]]
        ])
      )

    assert res == true
  end

  test "valid? data - true - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["name"], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 100])
      |> Dsv.valid?()

    assert res == true
  end

  @tag reusable_pipeline: true
  test "valid? data - true - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["name"], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 100])
      |> Dsv.valid?(state[:data_to_validate])

    assert res == true
  end

  test "valid? data - false", state do
    res =
      Dsv.valid?(
        state[:data_to_validate],
        paths([
          ["name", length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          ["age", number: [gt: 10, lt: 30]]
        ])
      )

    assert res == false
  end

  test "valid? data - false - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["name"], length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 30])
      |> Dsv.valid?()

    assert res == false
  end

  test "valid? data - false - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["name"], length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 30])
      |> Dsv.valid?(state[:data_to_validate])

    assert res == false
  end

  test "valid? data with list - true", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          ["name", length: [min: 1, max: 10], format: ~r/(B|b)r[a-z]{3,5}k/],
          ["age", number: [gt: 10, lt: 40]],
          ["programming_languages", position: ["0": [equal: "PHPa"]]]
        ])
      )

    assert res ==
             {:error,
              %{
                "name" => ["Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"],
                "programming_languages" => [%{:"0" => ["Values must be equal"]}]
              }}
  end

  test "valid? data with list - true - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["name"], length: [min: 1, max: 10], format: ~r/(B|b)r[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 40])
      |> Dsv.add_validator(["programming_languages"], position: ["0": [equal: "PHPa"]])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "name" => ["Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"],
                "programming_languages" => [%{:"0" => ["Values must be equal"]}]
              }}
  end

  test "valid? data with list - true - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["name"], length: [min: 1, max: 10], format: ~r/(B|b)r[a-z]{3,5}k/)
      |> Dsv.add_validator(["age"], number: [gt: 10, lt: 40])
      |> Dsv.add_validator(["programming_languages"], position: ["0": [equal: "PHPa"]])
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "name" => ["Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"],
                "programming_languages" => [%{:"0" => ["Values must be equal"]}]
              }}
  end
end

defmodule Dsv.ValidatorStructTest do
  use ExUnit.Case

  import Dsv, only: [paths: 1]

  setup_all do
    {:ok,
     data_to_validate: %User{
       name: "Bartek",
       age: 36,
       programming_languages: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
       skills: %{:programming => 100, :cooking => 60}
     }}
  end

  test "basic struct", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths(
          name: [length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/],
          age: [number: [gt: 10, lt: 100]]
        )
      )

    assert res == :ok
  end

  test "basic struct - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator([:name], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator([:age], number: [gt: 10, lt: 100])
      |> Dsv.validate()

    assert res == :ok
  end

  test "basic struct - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator([:name], length: [min: 1, max: 10], format: ~r/B|b[a-z]{3,5}k/)
      |> Dsv.add_validator([:age], number: [gt: 10, lt: 100])
      |> Dsv.validate(state[:data_to_validate])

    assert res == :ok
  end

  test "basic struct - multiple errors", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths(
          name: [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          age: [number: [gt: 10, lt: 30]]
        )
      )

    assert res ==
             {:error,
              %{
                :age => ["Value 36 must be lower than: 30 and greater than: 10"],
                :name => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ]
              }}
  end

  test "basic struct - multiple errors - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator([:name], length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/)
      |> Dsv.add_validator([:age], number: [gt: 10, lt: 30])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                :age => ["Value 36 must be lower than: 30 and greater than: 10"],
                :name => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ]
              }}
  end

  test "basic struct - multiple errors - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator([:name], length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/)
      |> Dsv.add_validator([:age], number: [gt: 10, lt: 30])
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                :age => ["Value 36 must be lower than: 30 and greater than: 10"],
                :name => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ]
              }}
  end
end

defmodule Dsv.ValidatorNestedMapTest do
  use ExUnit.Case

  import Dsv, only: [paths: 1]

  setup_all do
    {:ok,
     data_to_validate: %{
       "personal_data" => %{
         "name" => "Bartek",
         "age" => 36,
         "address" => %{
           "country" => "Poland",
           "city" => "Poznań"
         }
       },
       "other" => %{
         "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
         "skills" => %{
           :programming => 100,
           :cooking => 60
         },
         :interests => %{
           "trekking" => 100,
           "climbing" => 40,
           "playing_football" => 100
         }
       }
     }}
  end

  test "valid? data within list - false", state do
    res =
      Dsv.validate(state[:data_to_validate], %{
        "personal_data" => %{
          "name" => [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          "age" => [number: [gt: 10, lt: 30]],
          "address" => %{
            "city" => [length: [min: 10, max: 20]]
          }
        },
        "other" => %{
          :interests => %{
            "trekking" => [number: [lt: 20]]
          },
          "programming_languages" => [
            position: ["2": [equal: "Scala", format: ~r/^S[0-9]*$/]],
            length: [min: 20]
          ]
        }
      })

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => [
                    %{
                      :"2" => [
                        "Values must be equal",
                        "Value Java does not match pattern ^S[0-9]*$"
                      ]
                    },
                    "Value [\"PHP\", \"Python\", \"Java\", \"Go\", \"Elixir\", \"Kotlin\"] is to short. Minimum lenght is 20"
                  ]
                }
              }}
  end

  test "valid? data within list - false - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages"],
        position: ["2": [equal: "Scala", format: ~r/^S[0-9]*$/]],
        length: [min: 20]
      )
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => [
                    %{
                      :"2" => [
                        "Values must be equal",
                        "Value Java does not match pattern ^S[0-9]*$"
                      ]
                    },
                    "Value [\"PHP\", \"Python\", \"Java\", \"Go\", \"Elixir\", \"Kotlin\"] is to short. Minimum lenght is 20"
                  ]
                }
              }}
  end

  test "valid? data within list - false - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages"],
        position: ["2": [equal: "Scala", format: ~r/^S[0-9]*$/]],
        length: [min: 20]
      )
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => [
                    %{
                      :"2" => [
                        "Values must be equal",
                        "Value Java does not match pattern ^S[0-9]*$"
                      ]
                    },
                    "Value [\"PHP\", \"Python\", \"Java\", \"Go\", \"Elixir\", \"Kotlin\"] is to short. Minimum lenght is 20"
                  ]
                }
              }}
  end

  test "valid? data within list - true", state do
    res =
      Dsv.validate(state[:data_to_validate], %{
        "personal_data" => %{
          "name" => [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          "age" => [number: [gt: 10, lt: 30]],
          "address" => %{
            "city" => [length: [min: 10, max: 20]]
          }
        },
        "other" => %{
          :interests => %{
            "trekking" => [number: [lt: 20]]
          },
          "programming_languages" => [position: ["2": [equal: "Java"]]]
        }
      })

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "valid? data within list - true - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages"], position: ["2": [equal: "Java"]])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "valid? data within list - true - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages"], position: ["2": [equal: "Java"]])
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - false", state do
    res =
      Dsv.validate(state[:data_to_validate], %{
        "personal_data" => %{
          "name" => [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          "age" => [number: [gt: 10, lt: 30]],
          "address" => %{
            "city" => [length: [min: 10, max: 20]]
          }
        },
        "other" => %{
          :interests => %{
            "trekking" => [number: [lt: 20]]
          },
          "programming_languages" => %{
            "2" => [equal: "Scala"],
            "3" => [length: [min: 3, max: 5], format: ~r/^[0-9]+Hello.*$/]
          }
        }
      })

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => %{
                    "2" => ["Values must be equal"],
                    "3" => [
                      "Value \"Go\" has wrong length. Minimum lenght is 3, maximum length is 5",
                      "Value Go does not match pattern ^[0-9]+Hello.*$"
                    ]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - false - integer key for position validator",
       state do
    res =
      Dsv.validate(state[:data_to_validate], %{
        "personal_data" => %{
          "name" => [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          "age" => [number: [gt: 10, lt: 30]],
          "address" => %{
            "city" => [length: [min: 10, max: 20]]
          }
        },
        "other" => %{
          :interests => %{
            "trekking" => [number: [lt: 20]]
          },
          "programming_languages" => %{
            2 => [equal: "Scala"],
            3 => [length: [min: 3, max: 5], format: ~r/^[0-9]+Hello.*$/]
          }
        }
      })

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => %{
                    2 => ["Values must be equal"],
                    3 => [
                      "Value \"Go\" has wrong length. Minimum lenght is 3, maximum length is 5",
                      "Value Go does not match pattern ^[0-9]+Hello.*$"
                    ]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - false - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages", "2"], equal: "Scala")
      |> Dsv.add_validator(["other", "programming_languages", "3"],
        length: [min: 3, max: 5],
        format: ~r/^[0-9]+Hello.*$/
      )
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => %{
                    "2" => ["Values must be equal"],
                    "3" => [
                      "Value \"Go\" has wrong length. Minimum lenght is 3, maximum length is 5",
                      "Value Go does not match pattern ^[0-9]+Hello.*$"
                    ]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - false - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages", "2"], equal: "Scala")
      |> Dsv.add_validator(["other", "programming_languages", "3"],
        length: [min: 3, max: 5],
        format: ~r/^[0-9]+Hello.*$/
      )
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => %{
                    "2" => ["Values must be equal"],
                    "3" => [
                      "Value \"Go\" has wrong length. Minimum lenght is 3, maximum length is 5",
                      "Value Go does not match pattern ^[0-9]+Hello.*$"
                    ]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - false - pipeline - atom as a position for list validator",
       state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages", :"2"], equal: "Scala")
      |> Dsv.add_validator(["other", "programming_languages", :"3"],
        length: [min: 3, max: 5],
        format: ~r/^[0-9]+Hello.*$/
      )
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => %{
                    "2": ["Values must be equal"],
                    "3": [
                      "Value \"Go\" has wrong length. Minimum lenght is 3, maximum length is 5",
                      "Value Go does not match pattern ^[0-9]+Hello.*$"
                    ]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - false - reusable pipeline - atom as a position for list validator",
       state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages", :"2"], equal: "Scala")
      |> Dsv.add_validator(["other", "programming_languages", :"3"],
        length: [min: 3, max: 5],
        format: ~r/^[0-9]+Hello.*$/
      )
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  },
                  "programming_languages" => %{
                    "2": ["Values must be equal"],
                    "3": [
                      "Value \"Go\" has wrong length. Minimum lenght is 3, maximum length is 5",
                      "Value Go does not match pattern ^[0-9]+Hello.*$"
                    ]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - true", state do
    res =
      Dsv.validate(state[:data_to_validate], %{
        "personal_data" => %{
          "name" => [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          "age" => [number: [gt: 10, lt: 30]],
          "address" => %{
            "city" => [length: [min: 10, max: 20]]
          }
        },
        "other" => %{
          :interests => %{
            "trekking" => [number: [lt: 20]]
          },
          "programming_languages" => %{
            "2" => [equal: "Java"]
          }
        }
      })

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - true - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages", "2"], equal: "Java")
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "valid? data within list explicit position - true - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.add_validator(["other", "programming_languages", "2"], equal: "Java")
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "validate data stored in map - no errors", state do
    res =
      Dsv.validate(state[:data_to_validate], %{
        "personal_data" => %{
          "name" => [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          "age" => [number: [gt: 10, lt: 30]],
          "address" => %{
            "city" => [length: [min: 10, max: 20]]
          }
        },
        "other" => %{
          :interests => %{
            "trekking" => [number: [lt: 20]]
          }
        }
      })

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "validate data stored in map - no errors - experiment", state do
    res =
      ValidatorPipeline.run_validation(state[:data_to_validate], %{
        "personal_data" => %{
          "name" => [length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          "age" => [number: [gt: 10, lt: 30]],
          "address" => %{
            "city" => [length: [min: 10, max: 20]]
          }
        },
        "other" => %{
          :interests => %{
            "trekking" => [number: [lt: 20]]
          }
        }
      })

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "validate data stored in map - no errors - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "validate data stored in map - no errors - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [lt: 20])
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                },
                "other" => %{
                  :interests => %{
                    "trekking" => ["Value 100 must be lower than: 20"]
                  }
                }
              }}
  end

  test "validate data stored in map, path - no errors", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          [
            path: ["personal_data", "name"],
            length: [min: 1, max: 3],
            format: ~r/(B|b)r[a-z]{3,5}k/
          ],
          [path: ["personal_data", "age"], number: [gt: 10, lt: 30]],
          [path: ["personal_data", "address", "city"], length: [min: 10, max: 20]],
          [path: ["other", :interests, "trekking"], number: [gt: 20]]
        ])
      )

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                }
              }}
  end

  test "validate data stored in map, path - no errors - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [gt: 20])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                }
              }}
  end

  test "validate data stored in map, path - no errors - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [gt: 20])
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 10"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                }
              }}
  end

  test "validate data stored in map, path - errors, custom message", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          [
            path: ["personal_data", "name"],
            length: [min: 1, max: 3],
            format: ~r/(B|b)r[a-z]{3,5}k/
          ],
          [path: ["personal_data", "age"], number: [gt: 10, lt: 30]],
          [path: ["personal_data", "address", "city"], length: [min: 10, max: 20]],
          [path: ["other", :interests, "trekking"], number: [gt: 20]]
        ]),
        message: "Custom error message"
      )

    assert res == {:error, "Custom error message"}
  end

  test "validate data stored in map, path - errors, custom message - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [gt: 20])
      |> Dsv.set_custom_message("Custom error message")
      |> Dsv.validate()

    assert res == {:error, "Custom error message"}
  end

  test "validate data stored in map, path - errors, custom message - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [gt: 20])
      |> Dsv.set_custom_message("Custom error message")
      |> Dsv.validate(state[:data_to_validate])

    assert res == {:error, "Custom error message"}
  end

  test "validate data stored in map, path - errors, custom message from function", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          [
            path: ["personal_data", "name"],
            length: [min: 1, max: 3],
            format: ~r/(B|b)r[a-z]{3,5}k/
          ],
          [path: ["personal_data", "age"], number: [gt: 10, lt: 30]],
          [path: ["personal_data", "address", "city"], length: [min: 10, max: 20]],
          [path: ["other", :interests, "trekking"], number: [gt: 20]]
        ]),
        message: fn _data, _options, errors ->
          get_and_update_in(errors, ["personal_data", "age"], fn current ->
            {current, ["You can't look at my age. It is unpleasant."]}
          end)
          |> elem(1)
          |> (& &1).()
        end
      )

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["You can't look at my age. It is unpleasant."],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                }
              }}
  end

  test "validate data stored in map, path - errors, custom message from function - pipeline",
       state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [gt: 20])
      |> Dsv.set_custom_message(fn _data, _options, errors ->
        get_and_update_in(errors, ["personal_data", "age"], fn current ->
          {current, ["You can't look at my age. It is unpleasant."]}
        end)
        |> elem(1)
      end)
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["You can't look at my age. It is unpleasant."],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                }
              }}
  end

  test "validate data stored in map, path - errors, custom message from function - reusable pipeline",
       state do
    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 10, lt: 30])
      |> Dsv.add_validator(["personal_data", "address", "city"], length: [min: 10, max: 20])
      |> Dsv.add_validator(["other", :interests, "trekking"], number: [gt: 20])
      |> Dsv.set_custom_message(fn _data, _options, errors ->
        get_and_update_in(errors, ["personal_data", "age"], fn current ->
          {current, ["You can't look at my age. It is unpleasant."]}
        end)
        |> elem(1)
      end)
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["You can't look at my age. It is unpleasant."],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                  ],
                  "address" => %{
                    "city" => [
                      "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                    ]
                  }
                }
              }}
  end

  test "validators pipeline", state do
    data = state[:data_to_validate]

    res =
      Dsv.validation(data)
      |> Dsv.add_validator(["personal_data", "name"], length: [min: 2], format: ~r/[A-Z]{1}.*/)
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 18, lt: 150])
      |> Dsv.add_validator(["other", :interests], length: [min: 1])
      |> Dsv.validate()

    assert res == :ok
  end

  test "validators reusable pipeline", state do
    data = state[:data_to_validate]

    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"], length: [min: 2], format: ~r/[A-Z]{1}.*/)
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 18, lt: 150])
      |> Dsv.add_validator(["other", :interests], length: [min: 1])
      |> Dsv.validate(data)

    assert res == :ok
  end

  test "validators pipeline - fail", state do
    data = state[:data_to_validate]

    res =
      Dsv.validation(data)
      |> Dsv.add_validator(["personal_data", "name"], length: [min: 2], format: ~r/[I-Z]{1}.*/)
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 18, lt: 30])
      |> Dsv.add_validator(["other", :interests], length: [min: 1])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 18"],
                  "name" => ["Value Bartek does not match pattern [I-Z]{1}.*"]
                }
              }}
  end

  test "validators reusable pipeline - fail", state do
    data = state[:data_to_validate]

    res =
      Dsv.validation()
      |> Dsv.add_validator(["personal_data", "name"], length: [min: 2], format: ~r/[I-Z]{1}.*/)
      |> Dsv.add_validator(["personal_data", "age"], number: [gt: 18, lt: 30])
      |> Dsv.add_validator(["other", :interests], length: [min: 1])
      |> Dsv.validate(data)

    assert res ==
             {:error,
              %{
                "personal_data" => %{
                  "age" => ["Value 36 must be lower than: 30 and greater than: 18"],
                  "name" => ["Value Bartek does not match pattern [I-Z]{1}.*"]
                }
              }}
  end
end

defmodule Dsv.ValidatorComplexKeysMapTest do
  use ExUnit.Case

  import Dsv, only: [paths: 1]

  setup_all do
    {:ok,
     data_to_validate: %{
       ["personal_data", "name"] => "Bartek",
       ["personal_data", "age"] => 36,
       ["personal_data", "address", "country"] => "Poland",
       ["personal_data", "address", "city"] => "Poznań",
       ["other", "programming_languages"] => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
       ["other", "skills"] => %{:programming => 100, :cooking => 60},
       ["other", :interests, "trekking"] => 100,
       ["other", :interests, "climbing"] => 40,
       ["other", :interests, "playing_football"] => 100
     }}
  end

  test "validate data stored in map complex key - errors", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        paths([
          [["personal_data", "name"], length: [min: 1, max: 3], format: ~r/(B|b)r[a-z]{3,5}k/],
          [["personal_data", "age"], number: [gt: 10, lt: 30]],
          [["personal_data", "address", "city"], length: [min: 10, max: 20]],
          [["other", :interests, "trekking"], number: [gt: 20]]
        ])
      )

    assert res ==
             {:error,
              %{
                ["personal_data", "age"] => [
                  "Value 36 must be lower than: 30 and greater than: 10"
                ],
                ["personal_data", "name"] => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ],
                ["personal_data", "address", "city"] => [
                  "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                ]
              }}
  end

  test "validate data stored in map complex key - errors - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator([["personal_data", "name"]],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator([["personal_data", "age"]], number: [gt: 10, lt: 30])
      |> Dsv.add_validator([["personal_data", "address", "city"]], length: [min: 10, max: 20])
      |> Dsv.add_validator([["other", :interests, "trekking"]], number: [gt: 20])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                ["personal_data", "age"] => [
                  "Value 36 must be lower than: 30 and greater than: 10"
                ],
                ["personal_data", "name"] => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ],
                ["personal_data", "address", "city"] => [
                  "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                ]
              }}
  end

  test "validate data stored in map complex key - errors - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator([["personal_data", "name"]],
        length: [min: 1, max: 3],
        format: ~r/(B|b)r[a-z]{3,5}k/
      )
      |> Dsv.add_validator([["personal_data", "age"]], number: [gt: 10, lt: 30])
      |> Dsv.add_validator([["personal_data", "address", "city"]], length: [min: 10, max: 20])
      |> Dsv.add_validator([["other", :interests, "trekking"]], number: [gt: 20])
      |> Dsv.validate(state[:data_to_validate])

    assert res ==
             {:error,
              %{
                ["personal_data", "age"] => [
                  "Value 36 must be lower than: 30 and greater than: 10"
                ],
                ["personal_data", "name"] => [
                  "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                  "Value Bartek does not match pattern (B|b)r[a-z]{3,5}k"
                ],
                ["personal_data", "address", "city"] => [
                  "Value \"Poznań\" has wrong length. Minimum lenght is 10, maximum length is 20"
                ]
              }}
  end
end

defmodule Dsv.ValidatorCompareTwoFilds do
  use ExUnit.Case

  setup_all do
    {:ok,
     data_to_validate: %{
       :user => %{
         :login => %{
           :password => "abcd1234",
           :password_confirmation => "abcd1234"
         },
         :address => %{
           :country => "Poland",
           :city => "Warsaw",
           :phone_number => ["0099999", "0099999"]
         },
         :skills => %{
           :cooking => 9,
           :swimming => 20,
           :dancing => 1,
           :climbing => 11
         }
       }
     }}
  end

  test "validate same password", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        %{
          user: %{
            address: %{
              :country => [in: ["Poland", "Ukraine", "Latvia"]],
              :city => [format: ~r/[A-Z][A-Za-z]+/]
            },
            login: %{:password => [length: [min: 8, max: 100]]}
          }
        },
        [
          [[:user, :login, :password], [:user, :login, :password_confirmation], [:equal]]
        ]
      )

    assert res == :ok
  end

  test "validate elements on the list", state do
    res =
      Dsv.validate(
        state[:data_to_validate],
        %{
          user: %{
            address: %{
              :country => [in: ["Poland", "Ukraine", "Latvia"]],
              :city => [format: ~r/[A-Z][A-Za-z]+/]
            },
            login: %{:password => [length: [min: 8, max: 100]]}
          }
        },
        [
          [[:user, :login, :password], [:user, :login, :password_confirmation], [:equal]],
          [[:user, :address, :phone_number, 0], [:user, :address, :phone_number, 1], [:equal]],
          [[:user, :skills, :cooking], [:user, :skills, :swimming], [number: [:lt]]],
          [[:user, :skills, :cooking], [:user, :skills, :dancing], [number: [:gt]]],
          [[:user, :skills, :climbing], [:user, :skills, :dancing], [number: [:gte]]],
          [[:user, :skills, :climbing], [:user, :skills, :swimming], [number: [:lte]]]
        ]
      )

    assert res == :ok
  end

  test "validate same password - pipeline", state do
    res =
      Dsv.validation(state[:data_to_validate])
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.validate()

    assert res == :ok
  end

  test "validate same password - reusable pipeline", state do
    res =
      Dsv.validation()
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.validate(state[:data_to_validate])

    assert res == :ok
  end

  test "validate same password fail - different password confirmation", _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd1234",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validate(
        data_to_validate,
        %{
          user: %{
            address: %{
              :country => [in: ["Poland", "Ukraine", "Latvia"]],
              :city => [format: ~r/[A-Z][A-Za-z]+/]
            },
            login: %{:password => [length: [min: 8, max: 100]]}
          }
        },
        [
          [[:user, :login, :password], [:user, :login, :password_confirmation], [:equal]]
        ]
      )

    assert res == {:error, %{user: %{login: %{password: ["Values must be equal"]}}}}
  end

  test "validate same password fail - different password confirmation - custom message", _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd1234",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validate(
        data_to_validate,
        %{
          user: %{
            address: %{
              :country => [in: ["Poland", "Ukraine", "Latvia"]],
              :city => [format: ~r/[A-Z][A-Za-z]+/]
            },
            login: %{:password => [length: [min: 8, max: 100]]}
          }
        },
        [
          [[:user, :login, :password], [:user, :login, :password_confirmation], [:equal]]
        ],
        message: "Please provide correct data."
      )

    assert res == {:error, "Please provide correct data."}
  end

  test "validate same password fail - different password confirmation - pipeline", _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd1234",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validation(data_to_validate)
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.validate()

    assert res == {:error, %{user: %{login: %{password: ["Values must be equal"]}}}}
  end

  test "validate same password fail - different password confirmation - reusable pipeline",
       _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd1234",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validation()
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.validate(data_to_validate)

    assert res == {:error, %{user: %{login: %{password: ["Values must be equal"]}}}}
  end

  test "validate same password fail - different password confirmation - pipeline - custom message",
       _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd1234",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validation(data_to_validate)
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.set_custom_message("Please provide correct data.")
      |> Dsv.validate()

    assert res == {:error, "Please provide correct data."}
  end

  test "validate same password fail - different password confirmation - reusable pipeline - custom message",
       _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd1234",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validation()
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.set_custom_message("Please provide correct data.")
      |> Dsv.validate(data_to_validate)

    assert res == {:error, "Please provide correct data."}
  end

  test "validate same password fail - invalid password and different password confirmation",
       _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validate(
        data_to_validate,
        %{
          user: %{
            address: %{
              :country => [in: ["Poland", "Ukraine", "Latvia"]],
              :city => [format: ~r/[A-Z][A-Za-z]+/]
            },
            login: %{:password => [length: [min: 8, max: 100]]}
          }
        },
        [
          [[:user, :login, :password], [:user, :login, :password_confirmation], [:equal]]
        ]
      )

    assert res ==
             {:error,
              %{
                user: %{
                  login: %{
                    password: [
                      "Value \"abcd\" has wrong length. Minimum lenght is 8, maximum length is 100",
                      "Values must be equal"
                    ]
                  }
                }
              }}
  end

  test "validate same password fail - invalid password and different password confirmation - pipeline",
       _state do
    data_to_validate = %{
      :user => %{
        :login => %{
          :password => "abcd",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validation(data_to_validate)
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                user: %{
                  login: %{
                    password: [
                      "Value \"abcd\" has wrong length. Minimum lenght is 8, maximum length is 100",
                      "Values must be equal"
                    ]
                  }
                }
              }}
  end

  test "validate same password fail - invalid password and different password confirmation - reusable pipeline",
       _state do
    data_to_validate = %{
      :user => %{
        :id => "12345678-aaaa-ffff-2121-1234acdef128",
        :login => %{
          :password => "abcd",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        }
      }
    }

    res =
      Dsv.validation()
      |> Dsv.add_validator([:user, :address, :country], in: ["Poland", "Ukraine", "Latvia"])
      |> Dsv.add_validator([:user, :address, :city], format: ~r/[A-Z][A-Za-z]+/)
      |> Dsv.add_validator([:user, :login, :password], length: [min: 8, max: 100])
      |> Dsv.add_validator([:user, :login, :password], [:user, :login, :password_confirmation], [
        :equal
      ])
      |> Dsv.add_validator([:user, :id], uuid: :basic)
      |> Dsv.validate(data_to_validate)

    assert res ==
             {:error,
              %{
                user: %{
                  login: %{
                    password: [
                      "Value \"abcd\" has wrong length. Minimum lenght is 8, maximum length is 100",
                      "Values must be equal"
                    ]
                  }
                }
              }}
  end

  test "validate same password with binded values fail - invalid password and different password confirmation - pipeline",
       _state do
    require Dsv

    data_to_validate = %{
      :check_city => true,
      :user => %{
        :login => %{
          :password => "abcd",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        },
        :personal => %{
          :age => 34,
          :years_of_work => 10,
          :to_old_to_work => 120
        }
      }
    }

    res =
      Dsv.validation(data_to_validate)
      |> Dsv.bind([:check_city], :check_city)
      |> Dsv.bind([:user, :login, :password_confirmation], :password_2)
      |> Dsv.bind([:user, :personal, :years_of_work], :work_years)
      |> Dsv.bind([:user, :personal, :to_old_to_work], :to_old_to_work)
      |> Dsv.add_validator([:user, :address, :country],
        in: ["Poland", "Ukraine", "Latvia"],
        not_in: ["USA", "Canada", "Mexico"]
      )
      |> Dsv.add_validator([:user, :address, :city],
        format: ~r/[A-Z][A-Za-z]+/,
        not_empty: Dsv.binded(:check_city)
      )
      |> Dsv.add_validator([:user, :login, :password],
        length: [min: 8, max: 100],
        equal: Dsv.binded(:password_2)
      )
      |> Dsv.add_validator([:user, :personal, :age],
        number: [gt: Dsv.binded(:work_years), lt: Dsv.binded(:to_old_to_work)]
      )
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                user: %{
                  login: %{
                    password: [
                      "Value \"abcd\" has wrong length. Minimum lenght is 8, maximum length is 100",
                      "Values must be equal"
                    ]
                  }
                }
              }}
  end

  test "validate same password with binded values fail - invalid password and different password confirmation - reusable pipeline",
       _state do
    require Dsv

    data_to_validate = %{
      :check_city => true,
      :user => %{
        :login => %{
          :password => "abcd",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        },
        :personal => %{
          :age => 34,
          :years_of_work => 10,
          :to_old_to_work => 120
        }
      }
    }

    res =
      Dsv.validation()
      |> Dsv.bind([:check_city], :check_city)
      |> Dsv.bind([:user, :login, :password_confirmation], :password_2)
      |> Dsv.bind([:user, :personal, :years_of_work], :work_years)
      |> Dsv.bind([:user, :personal, :to_old_to_work], :to_old_to_work)
      |> Dsv.add_validator([:user, :address, :country],
        in: ["Poland", "Ukraine", "Latvia"],
        not_in: ["USA", "Canada", "Mexico"]
      )
      |> Dsv.add_validator([:user, :address, :city],
        format: ~r/[A-Z][A-Za-z]+/,
        not_empty: Dsv.binded(:check_city)
      )
      |> Dsv.add_validator([:user, :login, :password],
        length: [min: 8, max: 100],
        equal: Dsv.binded(:password_2)
      )
      |> Dsv.add_validator([:user, :personal, :age],
        number: [gt: Dsv.binded(:work_years), lt: Dsv.binded(:to_old_to_work)]
      )
      |> Dsv.validate(data_to_validate)

    assert res ==
             {:error,
              %{
                user: %{
                  login: %{
                    password: [
                      "Value \"abcd\" has wrong length. Minimum lenght is 8, maximum length is 100",
                      "Values must be equal"
                    ]
                  }
                }
              }}
  end

  test "validate same password with binded values fail - invalid password and different password confirmation",
       _state do
    require Dsv

    data_to_validate = %{
      :check_city => true,
      :user => %{
        :login => %{
          :password => "abcd",
          :password_confirmation => "abcd12345"
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        },
        :personal => %{
          :age => 34,
          :years_of_work => 10,
          :to_old_to_work => 120
        }
      }
    }

    validator = %{
      :user => %{
        :login => %{
          :password => [length: [min: 8, max: 100], equal: Dsv.binded(:password_2)]
        },
        :address => %{
          :country => [in: ["Poland", "Ukraine", "Latvia"], not_in: ["USA", "Canada", "Mexico"]],
          :city => [format: ~r/[A-Za-z]+/, not_empty: Dsv.binded(:check_city)]
        },
        :personal => %{
          :age => [number: [gt: Dsv.binded(:work_years), lt: Dsv.binded(:to_old_to_work)]]
        }
      }
    }

    binded_values = %{
      password_2: [:user, :login, :password_confirmation],
      check_city: [:check_city],
      work_years: [:user, :personal, :years_of_work],
      to_old_to_work: [:user, :personal, :to_old_to_work]
    }

    res = Dsv.validate(data_to_validate, validator, binded_values: binded_values)

    assert res ==
             {:error,
              %{
                user: %{
                  login: %{
                    password: [
                      "Value \"abcd\" has wrong length. Minimum lenght is 8, maximum length is 100",
                      "Values must be equal"
                    ]
                  }
                }
              }}
  end

  test "all validators types with binded values",
       _state do
    require Dsv

    data_to_validate = %{
      :check_city => true,
      :accepted_countries => ["Poland", "Ukraine", "Latvia"],
      :unaccepted_countries => ["USA", "Canada", "Mexico"],
      :can_not_work_after => ~D[2020-10-11],
      :user_name_format => ~r/^[A-Z][a-z]+/,
      :country_name_format => ~r/^[A-Z][a-z]+/,
      :accepted_countries_name_length => 7,
      :accepted_countries_max_name_length => 1,
      :email_domain => ~r/[a-z]+/,
      :user => %{
        :id => %{
          :type => :basic,
          :value => "21112323-1a2d-3f5c-6e8f-0a9c8d7e6fff"
        },
        :name => "Usertestname",
        :login => %{
          :email => "user@email.com",
          :password => "abcd",
          :password_confirmation => "abcd12345",
          :min_length => 8,
          :max_length => 100
        },
        :address => %{
          :country => "Poland",
          :city => "Warsaw"
        },
        :personal => %{
          :age => 34,
          :years_of_work => 10,
          :to_old_to_work => 120,
          :last_day_of_work => ~D[2020-10-10]
        }
      }
    }

    res =
      Dsv.validation(data_to_validate)
      |> Dsv.bind([:check_city], :check_city)
      |> Dsv.bind([:user, :login, :password_confirmation], :password_2)
      |> Dsv.bind([:user, :personal, :years_of_work], :work_years)
      |> Dsv.bind([:user, :personal, :to_old_to_work], :to_old_to_work)
      |> Dsv.bind([:user, :login, :min_length], :min_pass_len)
      |> Dsv.bind([:user, :login, :max_length], :max_pass_len)
      |> Dsv.bind([:accepted_countries], :accepted_countries)
      |> Dsv.bind([:unaccepted_countries], :unaccepted_countries)
      |> Dsv.bind([:can_not_work_after], :can_not_work_after)
      |> Dsv.bind([:user, :address, :country], :user_country)
      |> Dsv.bind([:user_name_format], :user_name_format)
      |> Dsv.bind([:country_name_format], :country_name_format)
      |> Dsv.bind([:accepted_countries_name_length], :accepted_countries_name_length)
      |> Dsv.bind([:accepted_countries_max_name_length], :accepted_countries_max_name_length)
      |> Dsv.bind([:email_domain], :accepted_email_domain)
      |> Dsv.bind([:user, :id, :type], :user_id_type)
      |> Dsv.add_validator([:user, :address, :country],
        or: [
          [in: Dsv.binded(:accepted_countries), not_in: Dsv.binded(:unaccepted_countries)],
          [in: Dsv.binded(:unaccepted_countries), not_in: Dsv.binded(:accepted_countries)]
        ]
      )
      |> Dsv.add_validator([:user, :address, :city],
        format: ~r/[A-Z][A-Za-z]+/,
        not_empty: Dsv.binded(:check_city)
      )
      |> Dsv.add_validator([:user, :login, :password],
        length: [min: Dsv.binded(:min_pass_len), max: Dsv.binded(:max_pass_len)],
        equal: Dsv.binded(:password_2)
      )
      |> Dsv.add_validator([:user, :personal, :age],
        number: [gt: Dsv.binded(:work_years), lt: Dsv.binded(:to_old_to_work)]
      )
      |> Dsv.add_validator([:user, :personal, :last_day_of_work],
        date: [max: Dsv.binded(:can_not_work_after)]
      )
      |> Dsv.add_validator([:accepted_countries],
        position: ["0": [equal: Dsv.binded(:user_country)]]
      )
      |> Dsv.add_validator([:user, :name], format: Dsv.binded(:user_name_format))
      |> Dsv.add_validator([:accepted_countries],
        all: [format: Dsv.binded(:country_name_format)],
        any: [length: [min: Dsv.binded(:accepted_countries_name_length)]],
        none: [
          length: [max: Dsv.binded(:accepted_countries_max_name_length)]
        ]
      )
      |> Dsv.add_validator([:user, :login, :email],
        email: [top_level_domain: [format: Dsv.binded(:accepted_email_domain)]]
      )
      |> Dsv.add_validator(
        [:user, :address, :country],
        or: [
          length: [max: Dsv.binded(:accepted_countries_name_length)],
          length: [max: Dsv.binded(:accepted_countries_max_name_length)]
        ]
      )
      |> Dsv.add_validator([:user, :id, :value], uuid: [format: Dsv.binded(:user_id_type)])
      |> Dsv.validate()

    assert res ==
             {:error,
              %{
                user: %{
                  login: %{
                    password: [
                      "Value \"abcd\" has wrong length. Minimum lenght is 8, maximum length is 100",
                      "Values must be equal"
                    ]
                  }
                }
              }}
  end
end
