defmodule Dsv.UniquePropertyTest do
  use ExUnit.Case, async: true
  use ExUnitProperties

  property "only unique values are allowed" do
    check all(unique_list <- uniq_list_of(integer())) do
      assert Dsv.validate(unique_list, unique: true) == :ok
    end
  end

  property "list with duplicates are not valid" do
    check all(list <- list_of(integer(), min_length: 1)) do
      assert Dsv.validate(list ++ list, unique: true) ==
               {:error, ["Not all elements are unique."]}
    end
  end
end
