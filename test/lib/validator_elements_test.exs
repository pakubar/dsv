defmodule ValidatorElementsTest do
  alias ValidatorElements.Validator
  use ExUnit.Case

  describe "walk through validators and data" do
    test "go through validators defined as map" do
      data_to_validate = %{
        "name" => "Bartek",
        "age" => 36,
        "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
        "skills" => %{
          :programming => 100,
          :cooking => 60
        }
      }

      validator = %{
        "name" => [length: [min: 1, max: 20], format: ~r/^[A-Z].*$/],
        "age" => [number: [gt: 18, lt: 200]],
        "programming_languages" => [length: [min: 3]],
        "skills" => %{
          :programming => [number: [gt: 90]]
        }
      }

      result = [
        %Validator{validators: [number: [gt: 18, lt: 200]], data: 36, path: ["age"]},
        %Validator{
          validators: [length: [min: 1, max: 20], format: ~r/^[A-Z].*$/],
          data: "Bartek",
          path: ["name"]
        },
        %Validator{
          validators: [length: [min: 3]],
          data: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
          path: ["programming_languages"]
        },
        %Validator{validators: [number: [gt: 90]], data: 100, path: ["skills", :programming]}
      ]

      assert ValidatorElements.go_through(validator, data_to_validate) == result
    end

    test "go through validators defined as %Validators{}" do
      data_to_validate = %{
        "name" => "Bartek",
        "age" => 36,
        "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
        "skills" => %{
          :programming => 100,
          :cooking => 60
        }
      }

      validator = %Dsv.Validators{
        data: data_to_validate,
        validators: [
          [path: ["name"], length: [min: 1, max: 20], format: ~r/^[A-Z].*$/],
          [path: ["age"], number: [gt: 18, lt: 200]],
          [path: ["programming_languages"], length: [min: 3]],
          [path: ["skills", :programming], number: [gt: 90]]
        ]
      }

      result = [
        %Validator{
          validators: [length: [min: 1, max: 20], format: ~r/^[A-Z].*$/],
          data: "Bartek",
          path: ["name"]
        },
        %Validator{validators: [number: [gt: 18, lt: 200]], data: 36, path: ["age"]},
        %Validator{
          validators: [length: [min: 3]],
          data: ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
          path: ["programming_languages"]
        },
        %Validator{validators: [number: [gt: 90]], data: 100, path: ["skills", :programming]}
      ]

      assert ValidatorElements.go_through(validator, data_to_validate) == result
    end
  end
end
