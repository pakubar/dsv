defmodule ValidatorPipelineTest do
  use ExUnit.Case

  describe "validator pipeline error result" do
    test "run validators with map error result" do
      data_to_validate = %{
        "name" => "Bartek",
        "age" => 36,
        "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
        "skills" => %{
          :programming => 100,
          :cooking => 60
        }
      }

      validator = %{
        "name" => [length: [min: 1, max: 20], format: ~r/^[A-Z].*$/],
        "age" => [number: [gt: 18, lt: 200]],
        "programming_languages" => [length: [min: 3]],
        "skills" => %{
          :programming => [number: [gt: 90]]
        }
      }

      assert ValidatorPipeline.run_validation(data_to_validate, validator) == :ok
    end

    test "run validators with map error result - fail" do
      data_to_validate = %{
        "name" => "Bartek",
        "age" => 36,
        "programming_languages" => ["PHP", "Python", "Java", "Go", "Elixir", "Kotlin"],
        "skills" => %{
          :programming => 100,
          :cooking => 60
        }
      }

      validator = %{
        "name" => [length: [min: 1, max: 3], format: ~r/^[a-z].*$/],
        "age" => [number: [gt: 18, lt: 20]],
        "programming_languages" => [length: [min: 3, max: 4]],
        "skills" => %{
          :programming => [number: [gt: 190]]
        }
      }

      assert ValidatorPipeline.run_validation(data_to_validate, validator) ==
               {:error,
                %{
                  "age" => ["Value 36 must be lower than: 20 and greater than: 18"],
                  "name" => [
                    "Value \"Bartek\" has wrong length. Minimum lenght is 1, maximum length is 3",
                    "Value Bartek does not match pattern ^[a-z].*$"
                  ],
                  "programming_languages" => [
                    "Value [\"PHP\", \"Python\", \"Java\", \"Go\", \"Elixir\", \"Kotlin\"] has wrong length. Minimum lenght is 3, maximum length is 4"
                  ],
                  "skills" => %{programming: ["Value 100 must be greater than: 190"]}
                }}
    end
  end
end
