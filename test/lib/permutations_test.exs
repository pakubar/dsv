defmodule PermutationsTest do
  use ExUnit.Case

  test "validate empty list permutation" do
    assert Permutations.permutations([]) == [[]]
  end

  test "validate list with one element permutations" do
    assert Permutations.permutations([:gt]) == [[:gt]]
  end

  test "validate list with multiple elements permutations" do
    assert Permutations.permutations([:lt, :gt, :eq]) == [
             [:lt, :gt, :eq],
             [:lt, :eq, :gt],
             [:gt, :lt, :eq],
             [:gt, :eq, :lt],
             [:eq, :lt, :gt],
             [:eq, :gt, :lt]
           ]
  end

  test "validate permutations for single atom element" do
    assert Permutations.permutations(:eq) == [[:eq]]
  end
end
