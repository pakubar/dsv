# Elixir Validator Library

**Simple validator for Elixir**

![Elixir Version](https://img.shields.io/badge/elixir-v1.16.0%2B-blue.svg)
![License](https://img.shields.io/badge/license-MIT-green.svg)

A versatile and extensible validation library for Elixir. Easily validate data for different data types and scenarios.

## Features

- Provides a set of validators out of the box.
- Allows for easy extension with custom validation rules.
- Supports various data types, including numbers, strings, dates, and more.
- Flexible and user-friendly API for integrating validation into your Elixir applications.

## Installation

Add the `validator` package to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:dsv, "~> 0.3.0"}
  ]
end
```

Then, run:

```bash
$ mix deps.get
```

## Basic usage

To use the `Dsv` validator library to validate your data, use `Dsv.validate/2` function:


```elixir
iex> Dsv.validate("Test name", length: [min: 3])
:ok
```

This is an example of checking if a string has minimum 3 graphemes.


Full description with examples is available in the function documentation `Dsv.validate/2`.


## Documentation

For detailed documentation and examples, please refer to the [library documentation](https://hexdocs.pm/dsv/how_to_validate.html).

## Contributing

Contributions are welcome! If you have suggestions, bug reports, or want to add new validation rules, please open an issue or submit a pull request.

## License

This library is released under the [MIT License](LICENSE).
